conda create -n nnenv python=3.6 -y
source activate nnenv
conda install pandas matplotlib jupyter scikit-learn numpy -y
pip install tensorflow

#Delete environment and remove all the packages
#conda remove -n nnenv --all -y

#Show environment lists
#conda env list
