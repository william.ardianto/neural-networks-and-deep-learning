# Exercise

In this exercise, you will help some tourists to identify the name of Malaysia foods when they passed by Petaling Street without asking people.  

https://colab.research.google.com/drive/1hpdZjL69xLbIpQx-0P5v8MJeaSl7bGYa

Open the link above and go to **File -> Save a copy in Drive...**

Set your notebook to run on GPU or TPU go to **Edit -> Notebook settings**

Objective:
- Train a model that can identify 5 different types of delicious Malaysia food with accuracy of at least 80%
