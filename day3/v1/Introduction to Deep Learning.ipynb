{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Deep Learning and Computer Vision\n",
    "*July 2018*\n",
    "\n",
    "*The Center of Applied Data Science*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='Table_of_Content'></a>\n",
    "\n",
    "* **[1. Introduction to Deep Learning](#introduction)**\n",
    " * [1.1. From Artificial Intelligence to Machine Learning and now, Deep Learning](#intro1)\n",
    " * [1.2. What is Deep Learning?](#intro2)\n",
    " * [1.3. Applications of Deep Learning](#intro3)\n",
    " * [1.4. Why do we need GPU for Deep Learning?](#intro4)\n",
    " * [1.5. Examples of Deep Learning Frameworks](#intro5)\n",
    "* **[2. Introduction to Computer Vision](#cv)**\n",
    " * [2.1. What is Computer Vision?](#compv3)\n",
    " * [2.2. Challenges in Computer Vision](#compv2)\n",
    " * [2.3. Applications of Vision and Image Processing](#compv3)\n",
    " * [2.4. Useful links and additional reading materials](#compv4)\n",
    "* **[3. Convolutional Neural Networks (ConvNets)](#convnet)**\n",
    " * [3.1. Three Types of Layers in Convolutional Neural Networks (ConvNets)](#convnet1)\n",
    " * [3.2. Zero-Padding](#convnet2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Introduction to Deep Learning<a name=\"introduction\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1. From Artificial Intelligence to Machine Learning and now, Deep Learning<a name=\"intro1\"></a>\n",
    "\n",
    "The field of Artificial Intelligence (AI) has paved the way for Machine Learning (ML) and currently, Deep Learning (DL), garnering much attention and interests from both novices and experts in the field. The evolution of computer science over the recent years has been astonishing; not forgetting the major breakthroughs achieved along the way. Let's take a look at the timeline involved: \n",
    "\n",
    "![](aidiagram.png)\n",
    "\n",
    "(Image Source: \n",
    "https://mse238blog.stanford.edu/2017/07/ramdev10/new-product-breakthroughs-with-recent-advances-in-deep-learning-and-future-business-opportunities/)\n",
    "\n",
    "### All About AI\n",
    "\n",
    "AI represents the broader aspects of computer science which encompasses of ML and DL. By definition, AI is the **effort to automate intellectual tasks normally performed by humans**. The introduction of AI can be traced back to as early as the 1950s; sparked by the idea of whether a **computer can be made to think**.\n",
    "\n",
    "While the superiority of AI in solving well-defined, logical problems such as that in a typical chess game remains justified, the same could not be concluded for more complex problems namely image classification, language translation as well as speech recognition. Realizing this, a new perspective is crucial and this brings about to the emergence of ML. \n",
    "\n",
    "### Second in Line: ML\n",
    "\n",
    "ML started to gain importance in the 1990s with accelerated progress and driven to fame due to the availability of larger datasets and faster hardware. The ultimate goal of both ML and DL is centred to **transform large datasets** and subsequently, to find **useful representations of data** through *“learning”* from pre-defined outputs and inputs. Since ML typically employs learning on *one or two layers of data representation*, ML is sometimes known as *“shallow learning”*. On the other hand, the uniqueness of DL lies in the emphasis of its *multiple layers of data representations*.\n",
    "\n",
    "Now that we are clear of these 2 terms (AI and ML), let's proceed with DL.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2. What is Deep Learning?<a name=\"intro2\"></a>\n",
    "\n",
    "DL is in fact a specific subfield of ML which denotes a **new take on generating useful and meaningful data representations** that emphasises on **learning through successive layers** in a model. This is also analogous as layered representations learning and hierarchical representations learning. The number of layers that contributes to the model is what we call as *\"depth\"*. While DL is certainly not a new field (its introduction can be dated back to the 1980s), it only rose to prominence in the early 20s by making significant advancements especially in areas that were previously challenging to ML. \n",
    "\n",
    "Modern DL focuses on *tens or even hundreds of successive layers*; learned automatically from exposure to large amount of training data. These layered representations are learned through models termed as **neural networks** which formed the **backbone to how DL works**. To give you a better visual understanding on what DL entails, let's have a look at the image below.\n",
    "\n",
    "![](deepnn.jpeg)\n",
    "\n",
    "(Image Source: https://medium.com/diaryofawannapreneur/deep-learning-for-computer-vision-for-the-average-person-861661d8aa61)\n",
    "\n",
    "Imagine having a multistage information-distillation process whereby a *large set of data (input)* undergoes several *successive filters (known as hidden layers)* to yield *useful, valuable data (output)* for a specific task. This somewhat summarizes how DL works!!!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.3. Applications of Deep Learning<a name=\"intro3\"></a>\n",
    "\n",
    "Tasks that were once deemed impossible are now achievable, all thanks to the rapid development of DL to help solve problems ranging from Computer Vision to natural language processing. The following table shows a non-exhaustive summary of the current applications of DL in the various fields of computer science:\n",
    "\n",
    "![](deep_learn.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.4. Why do we need GPU for Deep Learning?<a name=\"intro4\"></a>\n",
    "\n",
    "Central processing units (CPU) can have about 4-6 cores. CPU cores are much faster than graphical processor unit (GPU) cores, hence making them great at sequential tasks. GPU on the other hand usually have thousands of cores, e.g. Nvidia GTX 1080 Ti have 3584 cores. This makes them great at doing parallel tasks even though GPU cores are slower than CPU cores. In our first example today, you will see that using **CPU to train a simple networks can take 25x slower** than using GPU. \n",
    "\n",
    "Below is a sample of benchmarks of several convolutional neural network (ConvNet) architectures trained on a [CPU](https://ark.intel.com/products/83356/Intel-Xeon-Processor-E5-2630-v3-20M-Cache-2_40-GHz) and a GPU, with and without Nvidia's cuDNN library. In these benchmarks, with optimisation, using a GPU to train deep learning networks can speed up computation by 49x to 74x. \n",
    "\n",
    "![CPU vs GPU convnet benchmarks](Screen Shot 2018-07-23 at 9.37.09 AM.png)\n",
    "\n",
    "cuDNN 2.4-3.1 times faster than “unoptimised” CUDA\n",
    "\n",
    "(Source: https://github.com/jcjohnson/cnn-benchmarks)\n",
    "\n",
    "Visualisation adapted from Stanford CS231 2018 lecture slides \n",
    "\n",
    "Note: CUDA is a \"parallel computing platform and programming model developed by NVIDIA for general computing on graphical processing units (GPUs)\" [(source)](https://developer.nvidia.com/cuda-zone). cuDNN refers to the [NVIDIA CUDA® Deep Neural Network library](https://developer.nvidia.com/cudnn), which \"provides highly tuned implementations for standard routines such as forward and backward convolution, pooling, normalization, and activation layers\"."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.5. Examples of Deep Learning Frameworks <a name=\"intro5\"></a>\n",
    "\n",
    "The following lists some of the more popular and famous DL frameworks available:\n",
    "\n",
    "1. [Tensorflow](https://www.tensorflow.org)\n",
    "2. [CNTK](https://www.microsoft.com/en-us/cognitive-toolkit/) (The Microsoft Cognitive Toolkit) \n",
    "3. [Theano](http://deeplearning.net/software/theano/)\n",
    "1. [Torch](http://torch.ch/), [PyTorch](https://pytorch.org/) \n",
    "2. [Apache MXNet](https://mxnet.apache.org/)\n",
    "3. [Caffe](http://caffe.berkeleyvision.org/), [Caffe2](https://caffe2.ai/)\n",
    "\n",
    "To get a sense of how a low-level framework may differ from a high-level framework, consider PyTorch versus Keras; the two fastest-growing DL tools. Keras has a simpler Application Programming Interface (API), whereas PyTorch offers more flexibility and encourages users to understand concepts more deeply. Keras has more deployment options via Tensorflow, but PyTorch generally outperforms Keras in training speed. [(source)](https://deepsense.ai/keras-or-pytorch/)\n",
    "\n",
    "Let's see how DL can be applied in **Computer Vision** for image recognition and processing. \n",
    "\n",
    "Click here to go back [Table of Content](#Table_of_Content)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Introduction to Computer Vision<a name=\"cv\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1. What is Computer Vision?<a name = \"compv1\"></a>\n",
    "\n",
    "Computer Vision can be defined as an interdisciplinary field of science that **extracts certain-task specific information** from a **visual scene**; be it from an image or even a video. Unlike humans, computers perceive images as **picture elements or pixels**; represented by a **matrix of numbers** which stores their respective pixel values/intensities *(see image below)*. \n",
    "\n",
    "![](human_comp.png)\n",
    "\n",
    "(Image Adapted from:  https://hackernoon.com/mit-6-s094-deep-learning-for-self-driving-cars-2018-lecture-4-notes-computer-vision-f591f14b3b99)\n",
    "\n",
    "This clearly highlights the **huge discrepancy** between the 2 potential outputs albeit the image of the cat remains the same in both instances (human eye vs computer)!!! Hence, *\"teaching\"* a computer to *\"see\"* images the way we do is certainly an uphill task that requires continuous research and improvements!\n",
    "\n",
    "It is also wise to explore the various types of images and their colour distributions. This is essential for understanding some basics of Computer Vision before we look at image processing using DL:\n",
    "\n",
    "**a) Binary Image**\n",
    "\n",
    "- The typical *black and white* image\n",
    "- Has only 2 pixel values as implied by its name (0: black, 1: white)\n",
    "- No gray value present\n",
    "\n",
    "**b) Grayscale Image**\n",
    "\n",
    "- Most famous image format: 8 bit colour format\n",
    "- Pixel values range from 0 to 255\n",
    "\n",
    "**c) Colour Image**\n",
    "\n",
    "- Colour distribution is more complex than grayscale; expressed in the *Red-Green-Blue (RGB)* format\n",
    "- Commonly converted into grayscale image prior to further image processing techniques"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2. Challenges in Computer Vision<a name=\"compv2\"></a>\n",
    "\n",
    "Many researchers have concluded that Computer Vision as a field of research is notoriously difficult. As the **human visual system** is leaps and bounds **more superior** for most tasks such as facial and image recognition, it is not surprising that computer vision systems pales in comparison. Image recognition and processing may be an easy task for us but it would be **extremely tough** to *\"teach\"* a computer to perceive images just as we do. \n",
    "\n",
    "The following lists the common aspects that would cripple the performance of a computer in *\"learning\"* to *\"see\"* like humans:\n",
    "\n",
    "![](challenges.png)\n",
    "\n",
    "**a) Background clutter**\n",
    "\n",
    "- There may be instances where the image blends completely into the background; making image detection and recognition a tad more difficult\n",
    "- While we can easily spot the camouflaged dog by looking at the image closely, *\"teaching\"* a computer to do exactly the same may be an entirely different story all together\n",
    "\n",
    "**b) Illumination or lighting**\n",
    "\n",
    "- One of the most daunting tasks in Computer Vision is having to deal with low or poorly illuminated images \n",
    "- Although we can still correctly classify the object in this much darker image as a dog, computers might perceive this as something else and we certainly do not want this to happen!\n",
    "\n",
    "**c) Occlusion or hidden images**\n",
    "\n",
    "- Sometimes, we do not need to see the complete image before we can detect the target object \n",
    "- Part of the target image may be obstructed *(just like this one here!)* and this would pose great challenge for a computer to spot the dog from the not\n",
    "\n",
    "**d) Variations in viewpoints or pose**\n",
    "\n",
    "- Objects can also have different positions, poses or angles but that doesn't change the identity of the object\n",
    "- Although the image *(refer above)* obviously shows a dog, the change in pose would make the task of image recognition much more complex and laborious\n",
    "\n",
    "**That's not it!**\n",
    "\n",
    "Just when we thought we have seen it all... In fact, Computer Vision has also experienced **great failures** and **miserable flops** in image-related tasks over the recent years; hinting to yet another possible underlying challenge. One infamous example would be the misclassified black people as gorillas back in the year 2015 which has definitely disgruntled many. This has also sparked the on-going controversies which questioned the biasness of ML and Computer Vision in image recognition and classification. Computer Vision has appeared to be highly **biased towards people of colour** (darker skin tone) as these classifiers even worked better on people with fairer complexions. Therefore, it is crucial to ensure that the datasets are properly trained to minimize the effects of bias which can otherwise be a critical challenge towards effective image processing.  \n",
    "\n",
    "Despite the challenges faced, the field of Computer Vision driven by deep Neural Networks has seen outstanding progress and made stunning improvements as the years go by. With the availability of **large datasets** coupled with **high performing Graphics Processing Unit (GPU)**, DL networks utilized in Computer Vision can easily be iterated to gain rapid *\"learning\"* experiences. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.3. Applications of Vision and Image Processing<a name=\"compv3\"></a>\n",
    "\n",
    "In this modern era, the widespread use of Computer Vision in real-world applications is clearly visible. Let's have a look at some of this applications:\n",
    "\n",
    "![](CV_app.png)\n",
    "\n",
    "With this introductory knowledge on Neural Networks, DL and Computer Vision, we are now ready to explore the training of Neural Networks and handling datasets with respect to Computer Vision. \n",
    "\n",
    "Have fun coding and deep learning!!! :)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.4. Useful links and additional reading materials<a name=\"compv4\"></a>\n",
    "\n",
    "1) http://www.yaronhadad.com/deep-learning-most-amazing-applications/\n",
    "\n",
    "2) https://www.analyticsvidhya.com/blog/2016/04/deep-learning-computer-vision-introduction-convolution-neural-networks/\n",
    "\n",
    "3) https://www.infoq.com/presentations/unconscious-bias-machine-learning\n",
    "\n",
    "4) https://www.forbes.com/sites/mzhang/2015/07/01/google-photos-tags-two-african-americans-as-gorillas-through-facial-recognition-software/#5f9771d1713d\n",
    "\n",
    "Click here to go back [Table of Content](#Table_of_Content)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Convolutional Neural Networks (ConvNets)<a name = 'convnet'></a>\n",
    "\n",
    "\n",
    "## 3.1. Three Types of Layers in Convolutional Neural Networks (ConvNets)<a name='convnet1'></a>\n",
    "\n",
    "1. Convolutional block (CONV)\n",
    "2. Pooling (POOL)\n",
    "3. Fully connected (FC)\n",
    "\n",
    "Why use a convolutional layer? [[source]](http://ufldl.stanford.edu/tutorial/supervised/ConvolutionalNeuralNetwork/)\n",
    "1. CNN architecture takes advantage of the 2D structure of an input image using local connections and tied weights followed by some form of pooling, resulting in features that are invariant to translation. \n",
    "2. CNNs are easier to train and have many fewer parameters than fully connected networks with the same number of hidden units. \n",
    "\n",
    "The final layer is always a fully connected (dense) layer, but before the layer can be fully connected, the feature map must be flattened so that the output is in two dimensions. This is akin to a reshape operation such as `array.reshape(-1, height*width*depth)`. \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## 3.2. Zero-Padding <a name='convnet2'></a>\n",
    "\n",
    "* Helps to preserve height and widths of input/activation value as we go build deeper networks.\n",
    "* Added mostly for convolution operations.\n",
    "* Pad input with 0.\n",
    "* There are two variants of padding: (examples from this excellent [StackOverflow answer](https://stackoverflow.com/posts/39371113/revisions))\n",
    " * Valid convolution: No padding.\n"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    " In this example:\n",
    "\n",
    "    Input width = 13\n",
    "    Filter width = 6\n",
    "    Stride = 5\n",
    "\n",
    "inputs:         1  2  3  4  5  6  7  8  9  10 11 (12 13)\n",
    "               |________________|                dropped\n",
    "                              |_________________|"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " * Same convolution:Add padding size so that output size is the same with input size after convolution operation."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "               pad|                                      |pad\n",
    "   inputs:      0 |1  2  3  4  5  6  7  8  9  10 11 12 13|0  0\n",
    "               |________________|\n",
    "                              |_________________|\n",
    "                                              |________________|"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Click here to go back [Table of Content](#Table_of_Content)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
