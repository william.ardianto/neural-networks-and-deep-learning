# Awesome Neural Networks and Deep Learning

The material will cover the fundamental of neural networks (forward and backward propagation), activation functions, normalizations, regularizations, deeplearning frameworks (Tensorflow), and convolution neural networks (CNN).

## Table of contents

### Day 1: Neural Networks

- [x] Neural networks introduction
- [x] Forward propagation
- [x] Cost functions
- [x] Backward propagation
- [x] Gradient checking
- [x] Optimization (Gradient descent, Parameter update)

### Day 2: Neural Networks Extended

- [x] Parameters initialization
- [x] Data pre-processing
- [x] Activation functions
- [x] Batch normalizations
- [x] Regularizations
- [x] Advance optimization

### Day 3: Tensorflow

- [x] Tensorflow introduction (Tensor, Variable, Placeholder, Constant, Sessions)
- [x] Tensorflow data pipeline
- [x] CNN with Tensorflow low-level API
- [x] Tensorboard
- [x] CNN with Tensorflow high-level API (Keras and Estimator)
